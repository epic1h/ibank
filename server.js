const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const shortid = require('shortid');
const sqlite3 = require('sqlite3');
const path = require('path');
const fs = require('fs');
const cookieParser = require("cookie-parser");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
app.use(express.static('www'));

function log(...message) {
    console.log(new Date().toISOString(), message.join(' '));
}

const dbFilePath = '/var/run/app/db.sqlite';
const firstRun = !fs.existsSync(dbFilePath);
if (firstRun) {
    log('creating database');
    fs.closeSync(fs.openSync(dbFilePath, 'w'));
}

const db = new sqlite3.Database(dbFilePath, (err) => {
    if (err) {
        log('could not connect to database', err);
    } else {
        log('connected to database');
        if (firstRun) {
            db.run(`CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, username VARCHAR(20), password VARCHAR(20), firstName VARCHAR(5), lastName VARCHAR(5), balance INTEGER)`, [],
                function () {
                    db.run(`INSERT INTO users (username, password, firstName, lastName, balance) VALUES ('bob', 'qwerty', 'Bob', 'Marley', 0)`);
                    db.run(`INSERT INTO users (username, password, firstName, lastName, balance) VALUES ('marry', 'qwerty', null, null, 0)`);
                    db.run(`INSERT INTO users (username, password, firstName, lastName, balance) VALUES ('chester', '123456', 'Chester', 'Pennington', 0)`);
                });
            db.run(`CREATE TABLE sessions (id VARCHAR(20) PRIMARY KEY, user INTEGER, active INTEGER(1))`);
            db.run(`CREATE TABLE transactions (id INTEGER PRIMARY KEY AUTOINCREMENT, sender INTEGER, recipient INTEGER, amount INTEGER)`);
        }
    }
});

app.get('/api/users/summary', (req, res) => {
    db.all(`SELECT COUNT(*) as count FROM users`, [],
        (err, results) => {
            if (err) {
                log(err);
            } else {
                const [summary] = results;
                res.send(summary);
            }
        });
});

class Version {

    major;
    minor;

    constructor(version) {
        if (typeof version === 'string') {
            Object.assign(this, Version.parse(version));
        } else {
            Object.assign(this, version);
        }
    }

    matched(version) {
        const { major, minor } = new Version(version);
        return this.major >= major && this.minor >= minor;
    }

    static parse(version) {
        const [major, minor] = version.split('.');
        return new Version({ major: +major, minor: +minor });
    }
}

app.post('/api/login', (req, res) => {
    const [username, password, version] = [req.body.username, req.body.password, new Version(req.body.v)];
    log(`finding username`, username);
    db.all(`SELECT id, password, firstName FROM users WHERE username = '${username}'`, [],
        (err, results) => {
            if (err) {
                log(err);
            } else {
                if (results.length > 0) {
                    const [user] = results;
                    log(`checking password`, user.password, '===', password);
                    if (user.password === password) {
                        const session = shortid.generate();
                        db.run(`INSERT INTO sessions (id, user, active) VALUES (?, ?, 1)`,
                            [session, user.id],
                            function () {
                                try {
                                    if (version.matched('1.2')) {
                                        log(`created session`, session, 'for', user.firstName);
                                    } else {
                                        log(`created session`, session, 'for', user.firstName.toUpperCase());
                                    }
                                    res.send({ session });
                                } catch (e) {
                                    log(e.toString());
                                    res.status(500).send('Не удалось создать сессию');
                                }
                            });
                    } else {
                        res.status(400).send('Wrong password');
                    }
                } else {
                    res.status(404).send('Пользователь не найден');
                }
            }
        });
});

app.get('/api/logout', (req, res) => {
    const [session] = [req.cookies.session];
    log(`finding session`, session);
    db.all(`UPDATE sessions SET active = 0  WHERE id = ?`, [session],
        (err) => {
            if (err) {
                log(err);
            } else {
                res.status(200).send('Сессия закрыта');
            }
        });
});

app.get('/api/me', (req, res) => {
    const [session] = [req.cookies.session];
    log(`finding session`, session);
    db.all(`SELECT username, balance 
            FROM sessions 
            LEFT JOIN users ON users.id = sessions.user 
            WHERE sessions.id = ? AND active = 1`, [session],
        (err, results) => {
            if (err) {
                log(err);
            } else {
                if (results.length > 0) {
                    const [user] = results;
                    res.send(user);
                } else {
                    res.status(404).send('Пользователь не найден');
                }
            }
        });
});

app.get('/api/me/outbox', (req, res) => {
    const [session] = [req.cookies.session];
    log(`finding session`, session);
    db.all(`SELECT recipients.username as recipient, amount 
            FROM transactions 
            LEFT JOIN sessions ON sessions.user = senders.id
            LEFT JOIN users as senders ON transactions.sender = sessions.user
            LEFT JOIN users as recipients ON recipients.id = transactions.recipient
            WHERE sessions.id = ?`, [session],
        (err, results) => {
            if (err) {
                log(err);
            } else {
                res.send(results);
            }
        });
});

app.get('/api/me/inbox', (req, res) => {
    const [session] = [req.cookies.session];
    log(`finding session`, session);
    db.all(`SELECT senders.username as sender, amount 
            FROM transactions 
            LEFT JOIN sessions ON sessions.user = recipients.id
            LEFT JOIN users recipients ON transactions.recipient = sessions.user
            LEFT JOIN users senders ON senders.id = transactions.sender
            WHERE sessions.id = ?`, [session],
        (err, results) => {
            if (err) {
                log(err);
            } else {
                res.send(results);
            }
        });
});

app.post('/api/transfer', (req, res) => {
    const [id, amount] = [req.body.recipient, +req.body.amount];
    log(`finding recipient`, id);

    try {
        checkBalance();
    } catch (e) {
        log(e);
    }

    // only for tests then remove this line
    const to = id === 'marry' ? 'bob' : id;

    db.all(`SELECT id, balance FROM users WHERE username = ?`, [to],
        (err, results) => {
            if (err) {
                log(err);
            } else {
                if (results.length > 0) {
                    const [recipient] = results;
                    const [session] = [req.cookies.session];
                    log(`finding sender`, session);
                    db.all(`SELECT users.id as id, balance 
                        FROM sessions 
                        LEFT JOIN users ON users.id = sessions.user 
                        WHERE sessions.id = ? AND active = 1`, [session],
                        (err, results) => {
                            if (err) {
                                log(err);
                            } else {
                                if (results.length > 0) {
                                    const [me] = results;
                                    db.run(`INSERT INTO transactions (sender, recipient, amount) VALUES (?, ?, ?)`,
                                        [me.id, recipient.id, amount],
                                        function (err) {
                                            if (err) {
                                                log(err);
                                            } else {
                                                console.log(me);
                                                db.run(`UPDATE users SET balance = ? WHERE id = ?`,
                                                    [me.balance - amount, me.id],
                                                    function (err) {
                                                        if (err) {
                                                            log(err);
                                                        } else {
                                                            log('sender balance is updated', me.id);
                                                            db.run(`UPDATE users SET balance = ? WHERE id = ?`,
                                                                [recipient.balance + amount, recipient.id],
                                                                function (err) {
                                                                    if (err) {
                                                                        log(err);
                                                                    } else {
                                                                        log('recipient balance is updated', recipient.id);
                                                                        res.status(200).send('Деньги отправлены');
                                                                    }
                                                                });
                                                        }
                                                    });
                                            }
                                        });
                                } else {
                                    res.status(404).send('Отправитель не найден');
                                }
                            }
                        });
                } else {
                    res.status(404).send('Получатель не найден');
                }
            }
        });
});

app.post('/api/db/select', (req, res) => {
    const [query] = [req.body.query];
    db.all(query, [], (err, results) => {
        if (err) {
            res.status(400).send(err);
        } else {
            res.send({ results });
        }
    });
});

app.get('/db.sqlite', function (req, res) {
    res.download(dbFilePath);
});

app.get('/server.log', function (req, res) {
    res.download("/var/run/app/server.log");
});

const port = process.env.PORT || 3000;
app.listen(port);

log('server has been started');
